package com.twuc.webApp;

import java.util.HashMap;

public class Storage {

    private HashMap<Ticket, Bag> storage = new HashMap<>();

    private int smallCapacity;
    private int mediumCapacity;
    private int bigCapacity;
    private int usedSmallSlot;
    private int usedMediumSlot;
    private int usedBigSlot;

    public Storage(int smallCapacity, int mediumCapacity, int bigCapacity) {
        this.smallCapacity = smallCapacity;
        this.mediumCapacity = mediumCapacity;
        this.bigCapacity = bigCapacity;
    }

    public Storage(int smallCapacity) {
        this.smallCapacity = smallCapacity;
    }

    public static Storage createStorage(int capacity) {
        return new Storage(capacity);
    }

    public Ticket save(Bag bag) {
        if (usedSmallSlot < smallCapacity) {
            Ticket ticket = new Ticket();
            this.storage.put(ticket, bag);
            usedSmallSlot++;
            return ticket;
        }
        throw new IllegalArgumentException("Insufficient capacity");
    }

    public Ticket save(Bag bag, StorageSize slotSize) {
        if (bag.getSize().getSize()>slotSize.getSize()) {
            throw new IllegalArgumentException("Cannot save your bag");
        }
        switch (slotSize) {
            case BIG:
                if (usedBigSlot < bigCapacity) {
                    Ticket ticket = new Ticket();
                    this.storage.put(ticket, bag);
                    usedBigSlot++;
                    return ticket;
                }
                throw new IllegalArgumentException("Insufficient capacity");
            case MEDIUM:{
                if (usedMediumSlot < mediumCapacity) {
                    Ticket ticket = new Ticket();
                    this.storage.put(ticket, bag);
                    usedMediumSlot++;
                    return ticket;
                }}
                throw new IllegalArgumentException("Insufficient capacity");
            case SMALL: {
                if (usedSmallSlot < smallCapacity) {
                    Ticket ticket = new Ticket();
                    this.storage.put(ticket, bag);
                    usedSmallSlot++;
                    return ticket;
                }
                throw new IllegalArgumentException("Insufficient capacity");
            }
            default:
                throw new IllegalArgumentException("Insufficient capacity");
        }
    }


    public Bag retrieve(Ticket ticket) {
        if (storage.containsKey(ticket)) {
            Bag bag = storage.remove(ticket);
            usedSmallSlot--;
            return bag;
        }
        throw new IllegalArgumentException("Invalid Ticket");
    }

}
