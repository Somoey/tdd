package com.twuc.webApp;

public class Bag {

    private StorageSize size;

    public Bag(StorageSize size) {
        this.size = size;
    }

    public Bag() {
    }

    public StorageSize getSize() {
        return size;
    }
}
