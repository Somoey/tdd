package com.twuc.webApp;

public enum StorageSize {
    BIG(3),
    MEDIUM(2),
    SMALL(1);

    private int size;

    StorageSize(int size) {//构造函数取出来的就是枚举自定义的值
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
