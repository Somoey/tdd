package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ApplicationTest {

    @Test
    void should_return_a_ticket_when_saving_a_bag() {
        Storage storage = Storage.createStorage(2);
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        assertNotNull(ticket);
    }

    @Test
    void should_get_a_ticket_when_saving_nothing() {
        Storage storage = Storage.createStorage(2);
        final Bag nothing = null;
        Ticket ticket = storage.save(nothing);
        assertNotNull(ticket);
    }

    @Test
    void should_get_the_saved_bag_when_giving_a_valid_ticket() {

        Storage storage = Storage.createStorage(2);
        Bag bag = new Bag();
        Ticket ticket = storage.save(bag);
        Bag retrieve = storage.retrieve(ticket);
        assertSame(bag, retrieve);
    }

    @Test
    void should_get_the_first_bag_when_using_the_first_ticket() {
        Storage storage = Storage.createStorage(2);
        Bag firstBag = new Bag();
        Ticket firstTicket = storage.save(firstBag);
        storage.save(new Bag());

        Bag retrieve = storage.retrieve(firstTicket);
        assertSame(firstBag, retrieve);
    }

    @Test
    void should_get_an_error_message_when_using_an_invalid_ticket() {
        Storage storage = Storage.createStorage(2);
        Ticket invalidTicket = new Ticket();

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> storage.retrieve(invalidTicket));

        assertEquals("Invalid Ticket", exception.getMessage());
    }

    @Test
    void should_get_an_error_message_when_using_empty_ticket() {
        Storage storage = Storage.createStorage(2);

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> storage.retrieve(null));
        assertEquals("Invalid Ticket", exception.getMessage());
    }

    @Test
    void should_get_an_error_message_when_the_ticket_has_been_used() {
        Storage storage = new Storage(2);
        Ticket ticket = storage.save(new Bag());
        storage.retrieve(ticket);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> storage.retrieve(ticket));
        assertEquals("Invalid Ticket", exception.getMessage());
    }

    @Test
    void should_get_nothing_when_using_a_valid_ticket_save_nothing() {
        Storage storage = Storage.createStorage(2);
        Bag nothing = null;
        Ticket validTicket = storage.save(nothing);

        assertNull(storage.retrieve(validTicket));
    }

    @Test
    void should_get_a_ticket_when_saving_to_an_empty_storage_with_capacity_2() {
        Storage storage = Storage.createStorage(2);
        Ticket ticket = storage.save(new Bag());

        assertNotNull(ticket);
    }

    @Test
    void should_get_message_when_saving_to_a_full_storage() {
        Storage storage = Storage.createStorage(1);
        Ticket ticket = storage.save(new Bag());
        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> storage.save(new Bag()));
        assertNotNull(ticket);
        assertEquals("Insufficient capacity", illegalArgumentException.getMessage());
    }

    @Test
    void should_retrieve_and_save_to_a_full_storage() {
        Storage storage = Storage.createStorage(1);
        Ticket ticket = storage.save(new Bag());

        Bag bag = storage.retrieve(ticket);

        Ticket save = storage.save(new Bag());
        assertNotNull(save);
    }

    @Test
    void should_be_save_to_larger_slot() {
        Storage storage = new Storage(0, 0, 1);
        Bag smallBag = new Bag(StorageSize.SMALL);
        Ticket ticket = storage.save(smallBag, StorageSize.BIG);
        Bag retrieveBag = storage.retrieve(ticket);

        assertSame(smallBag, retrieveBag);
    }

    @Test
    void should_return_error_message_when_save_to_smaller_slot() {
        Storage storage = new Storage(1, 1, 1);

        IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> storage.save(new Bag(StorageSize.BIG), StorageSize.SMALL));
        assertEquals("Cannot save your bag", illegalArgumentException.getMessage());
    }

    @Test
    void should_return_error_message_when_save_to_full_big_storage() {
        Storage storage = new Storage(1, 1, 0);
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> storage.save(new Bag(StorageSize.MEDIUM), StorageSize.BIG));
        assertEquals("Insufficient capacity",exception.getMessage());
    }
}
